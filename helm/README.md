## helm
* create heml strcture
```
helm create graphql-quick-start
```
* execute heml charts
```
helm install graphql-release --debug --dry-run graphql-quick-start/

helm install graphql-release graphql-quick-start/

helm install graphql-release-dev graphql-quick-start/ --values graphql-quick-start/values.yaml -f graphql-quick-start/values-dev.yaml -n dev

```
* helm upgrade
```
helm upgrade graphql-release graphql-quick-start/ --values graphql-quick-start/values.yaml
```
* list
```
helm ls --all-namespaces
```
* delete
```
helm delete graphql-release-dev -n dev
```
* rollback
```
helm rollback graphql-release <version_no>
```
* template - shows charts
```
helm template graphql-quick-start
```
* lint - find any misconfiguration
```
helm lint graphql-quick-start
```

## helm-repo
* add repo
```
helm repo add graphql-quick-start git+https://gitlab.com/onkarantad/docker-k8s@k8s/helm/charts/graphql-quick-start?ref=main&sparse=0
```
* list repo
```
helm repo ls
```
* search charts
```
helm search repo graphql-quick-start
```
* install chart from repo
```
helm install graphql-release graphql-quick-start/graphql-quick-start
```
* update repo
```
helm repo update
```
* remove repo
```
helm repo remove graphql-quick-start
```

## helmfile
* sync
```
helmfile sync -f <helm-file>
```