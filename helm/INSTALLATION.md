## helm installation
```
wget https://get.helm.sh/helm-v3.15.3-linux-amd64.tar.gz

tar -xvf  helm-v3.15.3-linux-amd64.tar.gz

sudo mv linux-amd64/helm /usr/local/bin/helm

helm version
```
- [ ] [helm-Install-doc](https://helm.sh/docs/intro/install/#from-the-binary-releases)
 
## helmfile installation
```
wget https://github.com/helmfile/helmfile/releases/download/v0.167.1/helmfile_0.167.1_linux_amd64.tar.gz

tar -xvf  helmfile_0.167.1_linux_amd64.tar.gz

sudo mv helmfile /usr/local/bin/

helmfile version

```
- [ ] [helmfile-Install-doc](https://github.com/helmfile/helmfile?tab=readme-ov-file#installation)

## helm-git installation
```
helm plugin install https://github.com/aslafy-z/helm-git --version 1.3.0
```
- [ ] [helm-git-Install-doc](https://github.com/aslafy-z/helm-git?tab=readme-ov-file#helm-git)






