# Kubernetes Getting started
* Convert docker-compose.yml to k8s Deployement
```
kompose convert -f compose.yml
```
_it will generate deployement.yaml and service.yaml files_

* execute deployement and service in classpath
```
kubectl apply -f .
```
* execute seperately
```
kubectl apply -f <file>.yaml
```
* get services
```
kubectl get services -n <namespace>
```
* get pods
```
kubectl get pods -n <namespace>
```
* describe pod
```
kubectl describe pod <pod_name>
```
* get pod logs
```
kubectl logs <pod_name>
```
* get deployments
```
kubectl get deployments --all-namespaces
```
* delete deployments
```
kubectl delete deployment <deployment_name> -n <namespace>
```
* get into sell of pod
```
kubectl exec -it <podname> -- <shell>
```
***shell:***  `bash, sh, /bin/sh, /bin/bash`
* create namespace
```
kubectl create ns
```
* get namespace
```
kubectl get ns
kubectl get namespace
```
```
kubectl cluster-info  
```
* get persistance volume claim
```
kubectl get pvc <name> -n <namespace>
```
* get persistance volume
```
kubectl get pv <name> -n <namespace>
```
* delete pvc then pv
```
kubectl delete pvc <pvc-name>
kubectl delete pv <pv-name>
kubectl patch pvc graphql-quick-start-claim -p '{"metadata":{"finalizers": []}}' --type=merge
```
* encode string
```
echo -n 'your_string_here' | base64
```
* decode string
```
echo -n 'encoded_string_here' | base64 -d
```
* 
```

```
