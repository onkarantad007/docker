* Quick Start
```
kubectl create ns argocd
kubectl apply -n argocd -f https://raw.githubusercontent.com/argoproj/argo-cd/stable/manifests/install.yaml
```
- [ ] [argocd-install](https://argo-cd.readthedocs.io/en/stable/#quick-start)
* get UI
```
kubectl port-forward svc/argocd-server -n argocd --address 0.0.0.0 8085:443
```
* username & password
```
username: admin
password:
kubectl -n argocd get secret argocd-initial-admin-secret -o jsonpath="{.data.password}" | base64 -d ; echo

```
* if getting firewall issues
```
kubectl patch svc argocd-server -n argocd -p '{"spec": {"type": "LoadBalancer"}}'

sudo firewall-cmd --permanent --zone=public --add-port=8080/tcp 
sudo firewall-cmd --permanent --zone=public --add-port=443/tcp 
sudo firewall-cmd --reload
```
*
```

```