# Kubernetes Installation

* add user to docker group
```
sudo usermod -aG docker $USER && newgrp docker
```
* access docker indside k8s
```
eval $(minikube docker-env)
```

> ### ***Kubectl Installation***
1. Download the latest release with the command
```
curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"
`#optional` curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl.sha256"
`#optional` echo "$(cat kubectl.sha256)  kubectl" | sha256sum --check
sudo install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl
```
2. Install kubectl
```
chmod +x kubectl
mkdir -p ~/.local/bin
mv ./kubectl ~/.local/bin/kubectl
```
3. Test to ensure the version you installed is up-to-date
```
kubectl version --client
`#optional` kubectl version --client --output=yaml
```

***Ref:***
- [ ] [kubectl-linux](https://kubernetes.io/docs/tasks/tools/install-kubectl-linux/)

> ### ***Minikube Installation***
1. Install Minikube
```
curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64
sudo install minikube-linux-amd64 /usr/local/bin/minikube && rm minikube-linux-amd64
```
***Ref:***
- [ ] [minikube-linux](https://minikube.sigs.k8s.io/docs/start/?arch=%2Flinux%2Fx86-64%2Fstable%2Fbinary+download)

2. Start your cluster
```
minikube start --driver=docker
```
To make docker the default driver:
```
minikube config set driver docker
```
***Ref:***
- [ ] [docker-driver](https://minikube.sigs.k8s.io/docs/drivers/docker/)
4. check status
```
minikube status
```

3. Interact with your cluster
```
kubectl get po -A
```

> ### ***Kompose Installation***
Kompose is a conversion tool for Docker Compose to container orchestrators such as Kubernetes (or OpenShift).
```
# Linux
curl -L https://github.com/kubernetes/kompose/releases/download/v1.34.0/kompose-linux-amd64 -o kompose

# Linux ARM64
# curl -L https://github.com/kubernetes/kompose/releases/download/v1.34.0/kompose-linux-arm64 -o kompose

chmod +x kompose
sudo mv ./kompose /usr/local/bin/kompose
```
***Ref:***
- [ ] [kompose-doc](https://kubernetes.io/docs/tasks/configure-pod-container/translate-compose-kubernetes/)
